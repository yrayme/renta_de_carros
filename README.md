# Proyecto de Alquiler de Coches

Este proyecto es una aplicación de alquiler de coches donde el cliente puede registrarse, iniciar sesión, gestionar el alquiler y rellenar un formulario. También pueden ver su perfil y el historial de vehículos de alquiler. Esta aplicación está construida con React, Material UI, JavaScript y Redux.

## Descripción

La aplicación permite a los usuarios registrarse e iniciar sesión en su cuenta. Una vez dentro, pueden gestionar el alquiler de coches, rellenar un formulario de alquiler y ver su perfil y el historial de vehículos de alquiler.

## Tecnologías Utilizadas
- CSS, JavaScript
- Frameworks: React
- Firebase

## Instalación

Para ejecutar este proyecto localmente, sigue estos pasos:

1. Clona el repositorio en tu máquina local.
2. Instala las dependencias con `npm install`.
3. Inicia la aplicación con `npm start`.

## Uso

Para alquilar un coche, sigue estos pasos:

1. Regístrate o inicia sesión en tu cuenta.
2. Busca un coche disponible para alquilar.
3. Rellena el formulario de alquiler con los detalles necesarios.
4. Confirma el alquiler.

## Contribuir

Las contribuciones son bienvenidas. Por favor, abre un issue para discutir lo que te gustaría cambiar o añadir.
