import './App.css';
import React from 'react';
import { Switch, Route, Router } from "react-router-dom";
import { useSelector } from 'react-redux';
import history from './History';

/*Comnponents */
import Home from './Components/Home';
import Login from './Components/Login';
import Profile from './Components/Profile';
import Header from './Components/Header';
import RentalCars from './Components/RentalCars';
import FormRental from './Components/RentalCars/formRental';
import Footer from './Components/Footer';
import Alert from './Components/Alert';
import Account from './Components/Account'

import { ThemeProvider } from '@material-ui/styles';

import { CAR_THEME } from './Components/Home/constants';

const theme = CAR_THEME;

function App() {
  const alert = useSelector(state => state.alertReducer.alertPayload)
  const form = useSelector(state => state.carReducer.carPayload)

  return (
    <Router history={history}>
      <Header/>
      <Switch>
        <ThemeProvider theme={theme}>
          {form ? <FormRental /> :
            <>
              <Route path="/" exact component={Home} isPrivate />
              <Route path="/Login" exact component={Login} isPrivate />
              <Route path="/Profile" exact component={Profile} isPrivate />
              <Route path="/Account" exact component={Account} isPrivate />
              <Route path="/RentalCars" exact component={RentalCars} isPrivate />
            </>
          }
        </ThemeProvider>
      </Switch>
      <Footer />
      <Login />
      {alert && <Alert />}
    </Router>
  );
}

export default App;
