import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


// Your web app's Firebase configuration
 var firebaseConfig = {
    apiKey: "AIzaSyDHJXKkosQVbhP5p0nuHCvYS2IuR7iUmmA",
    authDomain: "carrental-react.firebaseapp.com",
    databaseURL: "https://carrental-react-default-rtdb.firebaseio.com",
    projectId: "carrental-react",
    storageBucket: "carrental-react.appspot.com",
    messagingSenderId: "739590510466",
    appId: "1:739590510466:web:9b280b0a26d7f6b5fe219b"
  };
  // Initialize Firebase
const fb= firebase.initializeApp(firebaseConfig);

export const db = fb.firestore();
export const auth = fb.auth();

