import { OPEN_LOGIN} from "../constants";


export const loginReducer = (state = {}, action) =>{
    switch(action.type) {
        case OPEN_LOGIN:
            return{ ...state,
                loginPayload: action.payload,
            };
    default:
        return state;
    }
}