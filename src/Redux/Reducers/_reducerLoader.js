import { LOADER} from "../constants";


export const loaderReducer = (state = {}, action) =>{
    switch(action.type) {
        case LOADER:
            return{ ...state,
                loaderPayload: action.payload,
            };
    default:
        return state;
    }
}