import { combineReducers } from 'redux';
import { RESET_ALL } from '../constants';
import { loginReducer } from './_reducerLogin';
import { loaderReducer } from './_reducerLoader';
import {carReducer} from './_reducerDb';
import { alertReducer } from './_reducerAlert';

const appReducer = combineReducers({
    loginReducer,
    loaderReducer,
    carReducer,
    alertReducer
});

const resetState = combineReducers({
    loginReducer,
    loaderReducer,
    carReducer,
    alertReducer
});

const rootReducer = (state, action) => {
    if (action.type === RESET_ALL) {
        state = undefined
        return resetState(state, action)
    } else {
        return appReducer(state, action)
    }
}

export default rootReducer;