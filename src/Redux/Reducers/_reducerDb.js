import { CAR, CARDB } from "../constants";


export const carReducer = (state = {}, action) => {
    switch (action.type) {
        case CAR:
            return {
                ...state,
                carPayload: action.payload,
                carProps: action.props,
            };
        case CARDB:
            return {
                ...state,
                carDbPayload: action.payload,
            };
        default:
            return state;
    }
}