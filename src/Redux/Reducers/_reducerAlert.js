import { ALERT } from "../constants";

export const alertReducer = (state = {}, action) =>{
    switch(action.type) {
        case ALERT:
            return{ ...state,
                alertPayload: action.payload,
                alertProps: action.props,
            };
    default:
        return state;
    }
}