import { OPEN_LOGIN} from '../constants';

export const openLogin = (payload) => {
    return{
        type: OPEN_LOGIN,
        payload: payload,
    }
}