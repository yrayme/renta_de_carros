import { LOADER} from '../constants';

export const loader = (payload) => {
    return{
        type: LOADER,
        payload: payload,
    }
}