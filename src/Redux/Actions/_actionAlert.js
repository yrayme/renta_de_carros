import { ALERT } from '../constants';

export const alertAction = (payload, props) => {
    return{
        type: ALERT,
        payload: payload,
        props: props
    }
}