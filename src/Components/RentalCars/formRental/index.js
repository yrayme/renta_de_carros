import React, { useState } from 'react';
import firebase from 'firebase';
import { useDispatch, useSelector } from 'react-redux';
import { TextField, Divider, Grid, Typography, Button } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import sedan from '../img/SedanPart.png';

/*Actions */
import { loader } from '../../../Redux/Actions/_actionLoader';
import { alertAction } from '../../../Redux/Actions/_actionAlert';

/*Constants */
import { styles } from '../constants.js';

export default function FormRental() {

    const classes = styles();
    const user = firebase.auth().currentUser;
    const nameCar = useSelector(state => state.carReducer.carPayload)
    const img = useSelector(state => state.carReducer.carProps)
    const dispatch = useDispatch();
    const [form, setForm] = useState({
        edad: '',
        nameCar: nameCar,
        img: img,
        ci: '',
        name: '',
        date: '',
        nDriver: '',
        uid: user.uid
    })
    const [error, setError] = useState('');

    //cambio de estado de los input
    const handleInputChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        })


    }

    //enviar datos del formulario
    const enviarForm = (event) => {
        event.preventDefault()
        var cont = 0;
        dispatch(loader(true));

        Object.keys(form).map(map => {
            if (form[map] === '') {
                cont++
            }
            return null;
        })

        if (cont > 0) {
            dispatch(loader(false));
            setError('Debe llenar todos los datos')
            return;
        } else {
            if (form['edad'] < 18) {
                dispatch(loader(false));
                setError('el usuario debe ser mayor de edad')

            } else {
                firebase.database().ref('form').push(form).then((data) => {
                    setError('')
                    dispatch(alertAction(
                        '¡Alquiler exitoso!',
                        `Se proceso su orden bajo el código de reserva ${data.key}`
                    ))

                    dispatch(loader(false));
                }).catch(() => {
                    dispatch(loader(false));
                });
            }
        }

    }

    return (
        <Grid container xs={12}>
            <Grid item xs={12} sm={12} md={12} spacing={2}>
                <Grid container className={classes.padding}>
                    <Grid xs={12} sm={4} md={4} style={{ backgroundColor: 'gray', }}>
                        <img src={sedan} alt='img' className={classes.img} />
                    </Grid>

                    <Grid container xs={12} sm={8} md={8} >
                        <Grid container xs={12} sm={12} md={12} style={{ marginTop: 30 }}>
                            <Grid xs={12} sm={1} md={1} />
                            <Grid xs={12} sm={11} md={11}>
                                <Grid xs={12} sm={12} md={12} container >
                                    <Typography variant='h3' color='primary' > ¡Formulario de alquiler!</Typography>
                                </Grid>
                                <Grid xs={12} sm={8} md={8} justify='center'  >
                                    <Divider style={{ backgroundColor: 'gray' }} />
                                </Grid>
                                {error &&
                                    <Grid xs={12} sm={10} md={10} justify='center'  >
                                        <Alert severity="error">{error}</Alert>
                                    </Grid>
                                }
                                <Grid xs={12} sm={12} md={12} container spacing={2} style={{ marginTop: 50 }}>
                                    <Grid xs={12} sm={5} md={5} item >
                                        <TextField
                                            required
                                            fullWidth
                                            label="Nombre y apellido"
                                            variant="outlined"
                                            name='name'
                                            value={form.name}
                                            onChange={handleInputChange}
                                        />
                                    </Grid>
                                    <Grid xs={12} sm={5} md={5} item >
                                        <TextField
                                            required
                                            id="date"
                                            label="Fecha"
                                            fullWidth
                                            variant="outlined"
                                            type="date"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            name='date'
                                            value={form.date}
                                            onChange={handleInputChange}
                                        />
                                    </Grid>
                                    <Grid xs={12} sm={5} md={5} item >
                                        <TextField
                                            required
                                            label="Edad"
                                            fullWidth
                                            variant="outlined"
                                            type='number'
                                            name='edad'
                                            value={form.edad}
                                            onChange={handleInputChange}
                                        />
                                    </Grid>
                                    <Grid xs={12} sm={5} md={5} item >
                                        <TextField
                                            disabled={true}
                                            required
                                            label="Nombre vehículo"
                                            fullWidth
                                            variant="outlined"
                                            name='nameCar'
                                            value={form.nameCar}
                                            onChange={handleInputChange}
                                        />
                                    </Grid>
                                    <Grid xs={12} sm={5} md={5} item >
                                        <TextField
                                            required
                                            label="N° de identificación"
                                            fullWidth
                                            variant="outlined"
                                            name='ci'
                                            value={form.ci}
                                            onChange={handleInputChange}
                                        />
                                    </Grid>
                                    <Grid xs={12} sm={5} md={5} item >
                                        <TextField
                                            required
                                            label="N° licencia"
                                            fullWidth
                                            variant="outlined"
                                            name='nDriver'
                                            value={form.nDriver}
                                            onChange={handleInputChange}
                                        />
                                    </Grid>
                                    <Grid xs={12} md={3} sm={3} container style={{ marginTop: 20, marginBottom: 20 }}>
                                        <Button variant="outlined"
                                            color="secondary"
                                            onClick={enviarForm}
                                        >
                                            Reservar
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}
