import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import firebase from 'firebase';
import Pagination from '@material-ui/lab/Pagination';

/*Components */
import Car from './Car';

/*Actions */
import { loader } from '../../Redux/Actions/_actionLoader';
/*CONSTANTES*/
import { styles, } from "./constants";
import { Grid } from '@material-ui/core';

export default function RentalCars() {

    const dispatch = useDispatch();
    const [cars, setCars] = useState([]);
    const [activeStep, setActiveStep] = useState(0);
    const [page, setPage] = useState(1);


    useEffect(() => {
        dispatch(loader(true));
        const database = firebase.database();
        var t2 = []
        database.ref().child("cars").get().then(function (snapshot) {
            if (snapshot.exists()) {
                const product = snapshot.val();
                if (window.innerWidth > 959) {
                    while (product.length > 0) { 
                        t2.push(product.splice(0, 8))
                    }
                } else if (window.innerWidth < 959 && window.innerWidth > 425) {
                    while (product.length > 0) {
                        t2.push(product.splice(0, 6))
                    }
                } else if (window.innerWidth <= 425) {
                    while (product.length > 0) {
                        t2.push(product.splice(0, 3))
                    }
                }
                dispatch(loader(false))
                setCars(t2);
            }
            else {
                    console.log("No data available");
                }
            }).catch(function (error) {
                console.log(error)
            });

    }, [dispatch]);

    const pagination = (event, value) => {
        setActiveStep(value - 1);
        setPage(value);
    }
    const classes = styles();
    return (
        <>
            {
                cars.length > 0 &&
                <Grid item xs={12} className={classes.fondo} >
                    <Grid container >
                        <Grid container xs={12} sm={12} md={12} justify='center' style={{ padding: 30 }}>
                            <Grid container className={classes.heightProduct} spacing={2}>
                                {
                                    cars.length > 0 &&
                                    <>
                                        {
                                            cars[activeStep].map(car => (
                                                <Grid item xs={12} sm={6} md={3} >
                                                    <Car
                                                        cars={car}
                                                        key={car.id} />
                                                </Grid>
                                            ))
                                        }
                                    </>
                                }

                            </Grid>
                        </Grid>

                        <Grid xs={12} sm={1} md={1} />

                    </Grid>
                    <Grid
                        container
                        justify="center"
                    >
                        <Pagination count={cars.length}
                            onChange={pagination} shape="rounded" color='primary'
                            page={page}
                            defaultPage={4}
                            siblingCount={0}
                            className={classes.pagination} />
                    </Grid>

                </Grid>
            }
        </>
    )
}
