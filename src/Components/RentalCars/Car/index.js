import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import firebase from 'firebase';
import { Card, CardActionArea, Divider, CardMedia, Typography, CardActions, Grid } from '@material-ui/core';

/*Components */

/*Actions */
import { carsAction } from '../../../Redux/Actions/_actionDb';

/*Constants */
import { styles } from '../constants.js';


export default function Car({ cars }) {

    const dispatch = useDispatch();
    const classes = styles();
    const [url, setUrl]= useState('')
    const { img, description, id, name } = cars;

    useEffect(() => {
        var storage = firebase.storage();
        var gsReference = storage.refFromURL(img)
        storage.ref().child(gsReference.fullPath).getDownloadURL().then(function (url) {
            setUrl(url)
        }).catch(function (error) {
        });
    }, [img]);

    const formDetails = () => {
        const database = firebase.database();
        database.ref().child("cars").child(id).get().then(function (snapshot) {
            if (snapshot.exists()) {
                const car = snapshot.val()
                dispatch(carsAction(car.name, car.img))
            }
            else {
                console.log("No data available");
            }
        }).catch(function (error) {
            console.error(error);
        });
    }

    return (
        <Grid container xs={12} md={12}>
            <Card className={classes.card}>
                <CardActionArea className={classes.CardActionArea}
                    onClick={formDetails.bind(id)}
                >
                    <CardMedia />
                    <img alt="img" src={url} width="350" className={classes.imgCard} />
                </CardActionArea>
                <Divider />
                <CardActions className={classes.colorCard} >
                    <Grid container>
                        <Grid xs={12} md={12} sm={12}>
                            <Typography color='primary' className={classes.title} >{name}</Typography>
                        </Grid>
                        <Grid xs={12} md={12} sm={12}>
                            <Typography className={classes.subtitle} >{description}</Typography>
                        </Grid>
                    </Grid>
                </CardActions>
            </Card>
        </Grid >
    );
}
