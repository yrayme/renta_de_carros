import { makeStyles } from '@material-ui/core/styles';
import fondo from './img/fondo.jpg';

export const styles = makeStyles((theme) => ({
    fondo:{
        backgroundImage: 'url('+ fondo + ')',
        backgroundSize: '100% 100%',
        backgroundRepeat: 'no-repeat',
        height:'100%'
    },
    pagination: {
        backgroundColor: 'gray',
        padding: 5,
        marginBottom: 15
    },

    card: {
        width: '100%',
        height: '350px',
        backgroundColor: '#f8f7f2'
    },

    CardActionArea: {
        height: 220,
        backgroundColor: "white",
    },
    colorCard: {
        backgroundColor: '#f8f7f2'
    },
    imgCard: {
        width: '100%',
        maxHeight: 270,
        maxWidth: 300,
        display: 'block',
        margin: 'auto',
        height:200
    },
    
    title: {
        fontSize: 20
    },
    subtitle: {
        fontSize: 14,
        textAlign:'justify'
    },

    img:{
        width: '100%',
        maxHeight: 500,
        maxWidth: 300,
        display: 'block',
        margin: 'auto',
        [theme.breakpoints.down('xs')]: {
            display: 'none'
        },
    },
    padding:{
        [theme.breakpoints.down('xs')]: {
            paddingLeft: '2vh'
        },
    }
}));