import { makeStyles } from '@material-ui/core/styles';

export const styles = makeStyles((theme) => ({

    title:{
        [theme.breakpoints.down('sm')]: {
            fontSize: 30,
        },
       
        [theme.breakpoints.up('md')]: {
            fontSize: "5rem"

        },
    }
}));