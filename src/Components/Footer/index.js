import React from 'react';
import { Grid, Typography, Divider } from '@material-ui/core';

/*Constants */
import { styles } from './constants';

export default function Footer() {
    const classes = styles();
    return (
        <Grid container style={{ backgroundColor: '#dddddd' }}>
            {/* <Grid item xs={0} md={2}></Grid> */}
            <Grid item xs={12} md={12} sm={12} style={{  textAlign: "center", margin: "auto" }}>
                <Divider style={{backgroundColor: '#1A5276'}}/>

                <Typography className={classes.title} >¡Alquila tu vehículo</Typography>
                <Typography className={classes.title}>de una forma rápida y sencilla!</Typography>
                <Typography variant="h4" style={{ textDecoration: "underline" }}>serviCar@servi.com.ve</Typography>
            </Grid>
            <Grid item xs={12} style={{ textAlign: "center", margin: "auto", marginTop:10 }} >
                <Typography >© 2021 - Yamile Rayme. | Políticas de privacidad</Typography>
            </Grid>

        </Grid>
    );
}