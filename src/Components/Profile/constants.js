import { makeStyles } from '@material-ui/core/styles';

export const styles = makeStyles((theme) => ({
  input: {
    display: 'none',
  },

  photo: {
    borderRadius: '50%',
    height: 200,
    width: '100%',
    maxHeight: 250,
    maxWidth: 250,
    display: 'block',
    margin: 'auto',
    [theme.breakpoints.down('xs')]: {

    },
  },
  img: {
    width: '100%',
    maxHeight: 600,
    maxWidth: 300,
    display: 'block',
    margin: 'auto',
    [theme.breakpoints.down('xs')]: {
        display: 'none'
    },
  },
  
  padding:{
    [theme.breakpoints.down('xs')]: {
        padding: '2vh'
    },
}
}));