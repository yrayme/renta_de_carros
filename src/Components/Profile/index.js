import React, { useState } from 'react';
import firebase from 'firebase';
import { useDispatch } from 'react-redux';
import { Grid, IconButton, LinearProgress, TextField, Typography, Divider, Button } from '@material-ui/core';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import sedan from './img/SedanPart.png';

/*Actions */
import { loader } from '../../Redux/Actions/_actionLoader';
import { alertAction } from '../../Redux/Actions/_actionAlert';

/*CONSTANTES*/
import { styles, } from "./constants";

export default function Profile() {

    const classes = styles();
    const dispatch = useDispatch();
    const user = firebase.auth().currentUser;
    const [cont, setCont] = useState(0);
    const [uploadValue, setuploadValue] = useState(0);
    const [picture, setPicture] = useState(user.photoURL);
    const [userData, setUserData] = useState({
        'displayName': user.displayName,
        'phoneNumber': user.phoneNumber
    });

    //cambio de estado de los input
    const handleInputChange = (event) => {
        setUserData({
            ...userData,
            [event.target.name]: event.target.value
        })
    }

    //actualizando datos del usuario
    const actualizarDatos = (event) => {
        event.preventDefault()
        dispatch(loader(true))
        console.log('enviando datos...' + userData.displayName + ' ' + userData.phoneNumber)
        user.updateProfile({
            displayName: userData.displayName,
            photoURL: picture,
            phoneNumber: userData.phoneNumber

        }).then(function () {
            dispatch(loader(false))
            dispatch(alertAction(
                '¡Actualización exitosa!',
                'Su usuario se ha actualizado exitosamente'
            ))
        }).catch(function (error) {
            dispatch(loader(false))
            // An error happened.
        });
    }

    //OBTENIENDO LA IMAGEN
    const changeImagen = e => {
        setCont(cont + 1)
        const file = e.target.files[0];
        var storage = firebase.storage();
        const storageref = storage.ref().child(user.email).child('foto' + cont);
        const task = storageref.put(file);

        var pathReference = storage.ref(storageref.fullPath);

        task.on('state_changed', snapshot => {
            let percentaje = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            setuploadValue(percentaje)
        }, error => {
        }, () => {
            setuploadValue(100)
            storage.ref().child(pathReference.fullPath).getDownloadURL().then(function (url) {
                setPicture(url);

            }).catch(function (error) {
                // Handle any errors
            });

        })
        setuploadValue(0)
    }

    return (
        <Grid container xs={12}>
            <Grid item xs={12} sm={12} md={12}>
                <Grid container className={classes.padding}>
                    <Grid xs={12} sm={4} md={4} style={{ backgroundColor: 'gray', }}>
                        <img src={sedan} alt='img' className={classes.img} />
                    </Grid>

                    <Grid container xs={12} sm={8} md={8} >
                        <Grid container xs={12} sm={12} md={12} style={{ marginTop: 20 }}>
                            <Grid xs={12} sm={1} md={1} />
                            <Grid xs={12} sm={11} md={11}>
                                <Grid xs={12} sm={12} md={12} container >
                                    <Typography variant='h4' color='primary' > Datos de usuario </Typography>
                                </Grid>
                                <Grid xs={12} sm={10} md={10} justify='center'  >
                                    <Divider style={{ backgroundColor: 'gray' }} />
                                </Grid>

                                <Grid xs={12} sm={12} md={12} container style={{ marginTop: 30 }}>
                                    {picture !== null &&
                                        <>
                                            <Grid xs={1} sm={12} md={12} />
                                            <Grid xs={9} sm={5} md={5} container justify='center'>
                                                <img src={picture} alt='img' width='300' className={classes.photo} style={{ objectFit: 'cover', width: '100%' }} />
                                            </Grid>
                                        </>

                                    }
                                    <Grid xs={9} sm={5} md={5} container justify='center' >
                                        {user.displayName !== null &&
                                            <Grid item xs={12} sm={12} md={12} className={classes.align} >

                                                <Typography variant='subtitle1' color='secondary' > <strong>Nombre de usuario:</strong> {user.displayName}</Typography>
                                            </Grid>
                                        }
                                        {user.email !== null &&
                                            <Grid item xs={12} sm={12} md={12} >

                                                <Typography variant='subtitle1' color='secondary' ><strong>correo electrónico:</strong>  {user.email}</Typography>
                                            </Grid>
                                        }
                                        {user.phoneNumber !== null &&
                                            <Grid item xs={12} sm={12} md={12} >

                                                <Typography variant='subtitle1' color='secondary' > Teléfono: {user.phoneNumber}</Typography>
                                            </Grid>
                                        }

                                    </Grid>
                                </Grid>
                                <Grid xs={12} sm={5} md={5} style={{ marginTop: 20 }}>
                                    <input name="imagen" className={classes.input} id="icon-button-file" type="file" onChange={changeImagen} />
                                    <label htmlFor="icon-button-file">
                                        <IconButton color="primary" aria-label="upload picture" component="span" >
                                            <PhotoCamera />
                                        </IconButton>
                                    </label>
                                    {uploadValue !== 0 &&
                                        <LinearProgress variant="determinate" value={uploadValue} />
                                    }
                                </Grid>
                                <Grid xs={12} sm={7} md={7} />

                                <Grid xs={12} sm={12} md={12} container spacing={1}>
                                    <Grid item xs={12} sm={5} md={5} style={{ marginTop: 30 }}>
                                        <TextField
                                            // required
                                            fullWidth
                                            label="Nombre usuario"
                                            variant="outlined"
                                            name='displayName'
                                            value={userData.displayName}
                                            onChange={handleInputChange}
                                        />

                                    </Grid>

                                    <Grid item xs={12} sm={5} md={5} style={{ marginTop: 30 }}>

                                    </Grid>

                                    <Grid xs={12} sm={2} md={2} />

                                    <Grid xs={12} md={2} sm={2} container justify='center' style={{ marginTop: 20 }}>
                                        <Button variant="outlined"
                                            color="secondary"
                                            onClick={actualizarDatos}>
                                            {'Actualizar'}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}
