import { makeStyles } from '@material-ui/core/styles';

export const styles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: "flex",
    height: 224,
    float: "right",
    position: "absolute",
    top: 64,
    right: 0,
    [theme.breakpoints.down('xs')]: {
      top: 55,
    },
  },
  transform: {
    '-webkit-transform': 'rotate(-90deg)',
    '-moz-transform': 'rotate(-90deg)',
    '-o-transform': 'rotate(-90deg)',
    transform: "rotate(-90deg)",
    marginTop: "15vh",
    marginLeft: -45,
    paddingTop: 10,
    [theme.breakpoints.down('xs')]: {
      marginRight: -45,
    },
  },

  textColor: {
    input: { color: "white" },
    "& label.Mui-focused": {
      color: "white",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "white",
      color: "white",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "white",
        color: "white",
      },
      "&:hover fieldset": {
        borderColor: "white",
        color: "white",
      },
      "&.Mui-focused fieldset": {
        borderColor: "white",
        color: "white",
      },
    },
  },


  paper: {
    borderRadius: "0px",
    minHeight: "60vh",
    width: 300,
    background: "linear-gradient(to bottom, #bdc3c7, #2c3e50)",
  },

  paperRegister: {
    borderRadius: "0px",
    minHeight: "80vh",
    width: 300,
    background: "linear-gradient(to bottom, #bdc3c7, #2c3e50)",
  },

  tabs: {
    minHeight: "60vh",
    width: 55,
    backgroundColor: 'gray',
  },

  buttonStyle: {
    border: "1px solid #820000",
    width: "40%",
    padding: 5,
    borderRadius: '0px',
    color: 'white'
  },

  password:{
    color: 'white', 
    textDecoration: 'underline white' ,
    "&:hover ": {
      cursor: 'pointer'
    },
  }
}));

///////////////////// JSON-INICIO DE SESIÓN //////////////////////////////

export const LOGINJSON = {
  tabs: {
    minHeight: "60vh",
    width: 55,
    backgroundColor: 'gray',
  },
};

//////////////// JSON-REGISTRAR///////////

export const REGISTERJSON = {
  tabs: {
    minHeight: "80vh",
    width: 55,
    backgroundColor: 'gray',
  },
};
