import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Slide, Tabs, Button, Tab, Typography, Box } from '@material-ui/core';
import CloseIcon from "@material-ui/icons/Close";

/**Components */
import Sesion from './Sesion';

/*Actions */
import { openLogin } from '../../Redux/Actions/_actionLogin';

/*CONSTANTES*/
import { styles, LOGINJSON, REGISTERJSON } from "./constants";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}


function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

function Login() {
  const [value, setValue] = useState(1);
  const [json, setJson] = useState(LOGINJSON)


  const dispatch = useDispatch();
  
  const openLoginModal = useSelector(state => state.loginReducer);   


  const handleChange = (event, newValue) => {
    if (newValue === 2) {
      setJson(REGISTERJSON);
    } else {
      setJson(LOGINJSON);
    }
    setValue(newValue);
  };

  const handleClose = () => {
    dispatch(openLogin(false));
  };


  /*Estilos */
  const classes = styles();

  return (

    <Slide
      direction="left"
      timeout={500}
      in={openLoginModal.loginPayload}
      mountOnEnter
      unmountOnExit
    >
      <div
        className={classes.root}
      >

        <Tabs
          orientation="vertical"
          indicatorColor="primary"
          value={value}
          onChange={handleChange}
          textColor="primary"
          aria-label="Vertical tabs example"
          style={json.tabs}
        >
          <Button onClick={handleClose}>
            <CloseIcon style={{ color: 'white', }} />
          </Button>
          <Tab
            label='INICIAR SESIÓN'
            className={classes.transform}
            {...a11yProps(0)}
          />
          <Tab
            label='REGISTRATE'
            className={classes.transform}
            {...a11yProps(1)}
          />
        </Tabs>
        <TabPanel value={value} index={1} className={classes.paper}>
          <Sesion login={1}/>
        </TabPanel>
        <TabPanel value={value} index={2} className={classes.paperRegister}>
          <Sesion login={2}/>
        </TabPanel>
      </div>

    </Slide>
  );
}


export default Login


