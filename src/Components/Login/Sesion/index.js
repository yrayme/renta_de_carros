import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Typography, Grid, TextField, Button, Divider, FormControl, FormControlLabel, Checkbox } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { auth } from '../../../firebase/firebase';

/*Actions */
import { loader } from '../../../Redux/Actions/_actionLoader';
import { accountAction } from '../../../Redux/Actions/_actionDb';

/*CONSTANTES*/
import { styles, } from "../constants";

function Sesion({ login }) {

    const dispatch = useDispatch();

    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const [error, setError] = useState('');
    const [boolPass, setBoolPass] = useState(false);

    const registrarUsuario = (e) => {
        e.preventDefault();
        if (!email.trim() || !pass.trim()) {
            console.log('Datos vacíos email!')
            setError('Datos vacíos email!')
            return
        }
        if (!pass.trim()) {
            console.log('Datos vacíos pass!')
            setError('Datos vacíos pass!')
            return
        }
        if (pass.length < 6) {
            console.log('6 o más carácteres')
            setError('6 o más carácteres en pass')
            return
        }

        dispatch(loader(true))
        if (login === 2) {
            auth.createUserWithEmailAndPassword(email, pass)
                .then((res) => {
                    dispatch(accountAction(true))
                    dispatch(loader(false));
                    setError(false);
                })
                .catch((error) => {
                    dispatch(loader(false));
                    setError(error.message);
                    if (error.code === 'auth/email-already-in-use') {
                        setError('Usuario ya registrado...')
                        return
                    }
                    if (error.code === 'auth/invalid-email') {
                        setError('Email no válido')
                        return
                    }
                })

        } else if (login === 1 && boolPass === false) {
            auth.signInWithEmailAndPassword(email, pass)
                .then((user) => {
                    dispatch(accountAction(true))
                    dispatch(loader(false));
                    setError(false);
                })
                .catch((error) => {
                    dispatch(loader(false));
                    if (error.code === 'auth/user-not-found') {
                        setError('Usuario o contraseña incorrecta')
                    }
                    if (error.code === 'auth/wrong-password') {
                        setError('Usuario o contraseña incorrecta')
                    }
                });
        }else{
            auth.sendPasswordResetEmail(email).then(function(val) {
                dispatch(loader(false));
              }).catch(function(error) {
                // An error happened.
              });
        }

    }

    const passWord = () => {
        setBoolPass(true);
    }
    
    const classes = styles();
    return (
        <Grid container xs={12} sm={12} md={12} justify='center' alignItems='center'>
            <Grid xs={12} sm={12} md={12} container >
                <Typography variant='h5' style={{ color: 'white' }} > ¡Bienvenido!</Typography>
            </Grid>
            <Grid xs={12} sm={12} md={12} justify='center'  >
                <Divider style={{ backgroundColor: 'white' }} />
                {error &&
                    <Alert severity="error">{error}</Alert>
                }
            </Grid>
            <Divider />
            <Grid xs={12} sm={12} md={12} container justify='center' alignItems='center' style={{ marginTop: 50 }}>
                <TextField id="standard-basic"
                    className={classes.textColor}
                    label="Correo electrónico"
                    type='email'
                    value={email}
                    onChange={(e) => { setEmail(e.target.value) }}
                />
            </Grid>
            {boolPass !== true &&
                <Grid xs={12} sm={12} md={12} container justify='center' alignItems='center' style={{ marginTop: 10 }}>
                    <TextField id="standard-basic"
                        label="Contraseña"
                        className={classes.textColor}
                        type="password"
                        value={pass}
                        onChange={(e) => { setPass(e.target.value) }} />
                </Grid>
            }
            {login === 2 ?
                <Grid xs={12} sm={12} md={12} container justify='center' alignItems='center' style={{ marginTop: 20 }}>
                    <FormControl component="fieldset">
                        <FormControlLabel
                            value="end"
                            control={<Checkbox color="primary" />}
                            label="Al iniciar sesión o crear una cuenta, aceptas nuestros Términos y condiciones y nuestra Declaración de privacidad."
                            labelPlacement="end"
                        />
                    </FormControl>
                </Grid>
                :
                <>
                    {boolPass !== true &&
                        <Grid xs={12} sm={12} md={12} container justify='center' alignItems='center' style={{ marginTop: 20 }}>
                            <Typography variant='subtitle2' className={classes.password} onClick={passWord} > ¿Reestablecer contraseña? </Typography>
                        </Grid>
                    }
                </>
            }



            <Grid xs={12} sm={12} md={12} container justify='center' style={{ marginTop: 25 }}>
                <Button
                    size="small"
                    variant="outlined"
                    color="primary"
                    className={classes.buttonStyle}
                    onClick={registrarUsuario}
                >

                    {login === 2 ? 'REGISTRAR' : boolPass === true ? 'RECUPERAR' : 'INGRESAR'}
                </Button>
            </Grid>
        </Grid>
    )
}

export default Sesion
