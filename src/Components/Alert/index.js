import React from 'react';
import { Typography, Dialog, DialogContent, DialogActions, Button, DialogTitle,
    DialogContentText, Divider } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

/*Actions */
import { alertAction } from '../../Redux/Actions/_actionAlert';
import { accountAction, carsAction } from '../../Redux/Actions/_actionDb';

export default function Alert() {
    const history = useHistory();
    const dispatch = useDispatch();

    const alert = useSelector(state => state.alertReducer)

    const save = () =>{
        dispatch(accountAction(true))
        dispatch(alertAction(false));
        dispatch(carsAction(false))
        history.push('/');
    }
    return (
        <Dialog
            open={alert.alertProps}
            keepMounted
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle style={{ fontSize: 20 }} id="alert-dialog-slide-title">
                <Typography style={{ fontSize: 20 }} color="primary">{alert.alertPayload}</Typography>
            </DialogTitle>
            <Divider style={{backgroundColor: '#1A5276'}}/>
            <DialogContent>
                <DialogContentText id="alert-dialog-slide-description" style={{ padding: '20px' }}>
                    {alert.alertProps}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button color="primary" onClick={save}
                    size="medium">
                    Aceptar
                </Button>
            </DialogActions>
        </Dialog>
    )
}
