import React, { useEffect, useState } from 'react';
import firebase from 'firebase';
import { Grid, Typography, Divider } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import Pagination from '@material-ui/lab/Pagination';
import sedan from './img/SedanPart.png';

/*Components */
import CardAccount from './CardAccount';


/*CONSTANTES*/
import { styles, } from "./constants";

export default function Account() {
    const classes = styles();
    const user = firebase.auth().currentUser;
    console.log(user)
    const [account, setAccount] = useState(null);
    const [activeStep, setActiveStep] = useState(0);
    const [page, setPage] = useState(1);


    useEffect(() => {
        var t2 = [];
        if (localStorage.getItem("account")) {
            var product = JSON.parse(localStorage.getItem("account"))
            if (window.innerWidth > 959) {
                while (product.length > 0) {
                    t2.push(product.splice(0, 2))
                }
            } else if (window.innerWidth < 959 && window.innerWidth > 425) {
                while (product.length > 0) {
                    t2.push(product.splice(0, 2))
                }
            } else if (window.innerWidth <= 425) {
                while (product.length > 0) {
                    t2.push(product.splice(0, 1))
                }
            }
            setAccount(t2);
        } else {
            setAccount(null);
        }
    }, []);

    const pagination = (event, value) => {
        setActiveStep(value - 1);
        setPage(value);
    }

    return (
        <>
            {
                account !== null ?
                    <Grid container xs={12}>
                        <Grid item xs={12} sm={12} md={12}>
                            <Grid container className={classes.padding}>
                                <Grid xs={12} sm={4} md={4} style={{ backgroundColor: 'gray', }}>
                                    <img src={sedan} alt='img' className={classes.img} />
                                </Grid>

                                <Grid container xs={12} sm={8} md={8} >
                                    <Grid container xs={12} sm={12} md={12} style={{ marginTop: 20 }}>
                                        <Grid xs={12} sm={1} md={1} />
                                        <Grid xs={12} sm={10} md={10}>
                                            <Grid xs={12} sm={12} md={12} container >
                                                <Typography variant='h4' color='primary' > Registro de vehículos rentados </Typography>
                                            </Grid>
                                            <Grid xs={12} sm={10} md={10} justify='center'  >
                                                <Divider style={{ backgroundColor: 'gray' }} />
                                            </Grid>

                                            <Grid xs={12} sm={12} md={12} container style={{ marginTop: 30 }}>
                                                <Grid container spacing={2}>
                                                    {
                                                        account.length > 0 &&
                                                        <>
                                                            {
                                                                account[activeStep].map(acc => (
                                                                    <Grid item xs={12} sm={12} md={12} >
                                                                        <CardAccount
                                                                            cuenta={acc}
                                                                            key={acc.key} />
                                                                    </Grid>
                                                                ))
                                                            }
                                                        </>
                                                    }
                                                </Grid>
                                            </Grid>
                                            {account.length > 0 &&
                                                <Grid
                                                    container
                                                    justify="center"
                                                >
                                                    <Pagination count={account.length}
                                                        onChange={pagination} shape="rounded" color='primary'
                                                        page={page}
                                                        defaultPage={4}
                                                        siblingCount={0}
                                                        className={classes.pagination} />
                                                </Grid>
                                            }
                                        </Grid>
                                    </Grid>

                                </Grid>

                            </Grid>
                        </Grid>
                    </Grid>
                    :
                    <Alert severity="error">Este usuario no posee vehículos rentados</Alert>
            }
        </>
    )
}
