import React from 'react';
import { Card, Typography, Grid } from '@material-ui/core';

/*Constants */
import { styles } from '../constants.js';

export default function CardAccount({ cuenta }) {
    const classes = styles();
    const { url, ci, edad, date, name, nameCar, nDriver, key } = cuenta
    return (
        <Card className={classes.card} variant="outlined" >
            <Grid container xs={12} md={12}>
                <Grid container xs={12} md={8} sm={8}>
                    <Grid xs={12} md={12} sm={12}>
                        <Typography color='primary'>Fecha: <span style={{ color: 'black' }}>{date}</span></Typography>
                    </Grid>
                    <Grid xs={12} md={12} sm={12}>
                        <Typography color='primary'>Código de reserva: <span style={{ color: 'black' }}>{key}</span></Typography>
                    </Grid>
                    <Grid xs={12} md={12} sm={12}>
                        <Typography color='primary' >Nombre y apellido: <span style={{ color: 'black' }}>{name}</span></Typography>
                    </Grid>
                    <Grid xs={12} md={12} sm={12}>
                        <Typography color='primary'>Edad: <span style={{ color: 'black' }}>{edad} años</span></Typography>
                    </Grid>
                    <Grid xs={12} md={12} sm={12}>
                        <Typography color='primary'>Número de identificación: <span style={{ color: 'black' }}>{ci}</span></Typography>
                    </Grid>
                    <Grid xs={12} md={12} sm={12}>
                        <Typography color='primary'>Número de licencia: <span style={{ color: 'black' }}>{nDriver}</span></Typography>
                    </Grid>
                    <Grid xs={12} md={12} sm={12}>
                        <Typography color='secondary'>Vehículo: <span style={{ color: 'black' }}>{nameCar}</span></Typography>
                    </Grid>
                </Grid>
                <Grid container xs={12} md={4} sm={4}>
                    <img alt="img" src={url}  className={classes.imgCar}/>
                </Grid>
            </Grid >
        </Card>
    )
}
