import { makeStyles } from '@material-ui/core/styles';

export const styles = makeStyles((theme) => ({

  img: {
    width: '100%',
    maxHeight: 600,
    maxWidth: 300,
    display: 'block',
    margin: 'auto',
    [theme.breakpoints.down('xs')]: {
      display: 'none'
    },
  },

  padding: {
    [theme.breakpoints.down('xs')]: {
      padding: '2vh'
    },

  },
  card:{
    padding: '20px'
  },

  imgCar: {
    width: '100%',
    maxHeight: 200,
    maxWidth: 400,
    display: 'block',
    margin: 'auto',
  },
  pagination: {
    backgroundColor: 'gray',
    padding: 5,
    marginBottom: 15,
    marginTop: 10
},

}));