import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { auth } from '../../firebase/firebase';
import { Link, useHistory } from 'react-router-dom';
import { Grid, AppBar, Toolbar, Button, IconButton, Typography, Menu, Fade, MenuItem, Avatar, LinearProgress } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';


/*Actions */
import { openLogin } from '../../Redux/Actions/_actionLogin';
import { carsAction } from '../../Redux/Actions/_actionDb';


/*Constants */
import { styles } from './constants';

export default function Header({ firebaseUser }) {
    const classes = styles();
    const history = useHistory();
    const dispatch = useDispatch();
    const [user, setUser] = useState(null);
    const [picture, setPicture] = useState(null)
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const loader = useSelector(state => state.loaderReducer.loaderPayload);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    useEffect(function () {
        auth.onAuthStateChanged(function (user) {
            if (user) {
                if (user.displayName !== null) {
                    setUser(user.displayName);
                } else {
                    setUser(user.email);
                }
                setPicture(user.photoURL)
                dispatch(openLogin(false));
            } else {
                setAnchorEl(null);
                setUser(null)

            }
        });
    })

    const login = (e) => {
        e.preventDefault();
        dispatch(openLogin(true));

    }

    const Logout = () => {
        auth.signOut().then(() => {
            setUser(null);
            dispatch(carsAction(false));
            localStorage.removeItem('account');
            history.push('/');
        }).catch((error) => {
        });
    }

    const alquiler = () => {
        if (user === null) {
            dispatch(openLogin(true));
        } else {
            dispatch(carsAction(false))
        }
    }

    const sinForm = () => {
        dispatch(carsAction(false))
    }

    return (
        <>
            <AppBar position="static" style={{ backgroundColor: '#0f0c29', boxShadow: "none" }}>
                <Toolbar>
                    <Grid container alignItems='center' style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                        <Grid item xs={12} sm={1} md={1} />
                        <Grid item xs={12} sm={4} md={6} style={{display: "flex", alignItems: "center"}}>
                            <Typography variant="h3" >
                                <Link to={'/'} style={{ textDecoration: 'none', color: 'white' }} onClick={sinForm}>ServiCar</Link>
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={6} md={4} style={{display: "flex", gap: 20, justifyContent: "flex-end", alignItems: "center"}} >
                            <div className={classes.alquiler}>
                                <Button onClick={alquiler}>
                                    <Typography style={{ color: 'white' }}>
                                        {user !== null ?
                                            <Link to={'/RentalCars'} style={{ textDecoration: 'none', color: 'white' }}>Gestionar Alquiler</Link>
                                            :
                                            'Gestionar Alquiler'
                                        }
                                    </Typography>
                                </Button>
                            </div>
                            <div>
                                {user !== null ?
                                    <div>
                                        <Button aria-controls="fade-menu" aria-haspopup="true" onClick={handleClick}>
                                            {picture !== null &&
                                                <Avatar alt="img" src={picture} />
                                            }
                                            <Typography variant='caption' style={{ color: 'white' }}>{user}</Typography>

                                        </Button>
                                        <Menu
                                            id="fade-menu"
                                            anchorEl={anchorEl}
                                            keepMounted
                                            open={open}
                                            onClose={handleClose}
                                            TransitionComponent={Fade}
                                        >
                                            <MenuItem className={classes.grid}><Link to={'/RentalCars'} style={{ textDecoration: 'none', color: 'black' }} onClick={sinForm}>Gestionar Alquiler</Link></MenuItem>
                                            <MenuItem ><Link to={'/Profile'} style={{ textDecoration: 'none', color: 'black' }} onClick={sinForm}>Perfil</Link></MenuItem>
                                            <MenuItem ><Link to={'/Account'} style={{ textDecoration: 'none', color: 'black' }} onClick={sinForm}>Mi cuenta</Link></MenuItem>
                                            <MenuItem onClick={Logout}>cerrar sesión</MenuItem>
                                        </Menu>
                                    </div>
                                    :
                                    <IconButton color="inherit" onClick={login}>
                                        <PersonIcon style={{ color: 'white' }} />
                                    </IconButton>
                                }
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={1} md={1} />
                    </Grid>
                </Toolbar>
            </AppBar>
            {loader &&
                <LinearProgress />
            }
        </>

    )
}

