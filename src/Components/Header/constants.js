import { makeStyles } from '@material-ui/core/styles';

export const styles = makeStyles((theme) => ({

    alquiler: {
        [theme.breakpoints.down('xs')]: {
            display: 'none',
        },
    },
    grid: {
        [theme.breakpoints.down('lg')]: {
            display: 'none',
        },
        [theme.breakpoints.down('xs')]: {
            display: 'block',
        },
    }
}));