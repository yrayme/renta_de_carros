import { makeStyles } from '@material-ui/core/styles';

export const styles = makeStyles((theme) => ({

    img: {
        width: '100%',
        maxHeight: '65vh',
        display: 'block',
        margin: 'auto',
        [theme.breakpoints.down('xs')]: {
            height: '40vh',
        },
    }
}));