import React from 'react';
import { Grid } from '@material-ui/core';
import "bootstrap/dist/css/bootstrap.min.css";
import car from '../img/cars.jpg';

/*Constants */
import { styles } from './constants';

function CarouselData() {

    const classes = styles();
    return (
        <Grid container>
            <Grid xs={12} sm={12} md={12}>
                <img
                    src={car}
                    alt="First slide"
                    className={classes.img}
                />
            </Grid>
        </Grid>
    )
}

export default CarouselData
