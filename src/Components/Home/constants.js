import { createMuiTheme } from '@material-ui/core/styles';


export const CAR_THEME = createMuiTheme({
  palette: {
    primary: {
      main: '#1A5276',
    },
    secondary: {
      main: '#820000',
    }
  },
});