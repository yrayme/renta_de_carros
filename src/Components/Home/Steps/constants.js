import { makeStyles } from '@material-ui/core/styles';

export const styles = makeStyles((theme) => ({
  card:{
      border: '1px solid #1A5276',
      background: 'linear-gradient(to right, #ada996, #f2f2f2, #dbdbdb, #eaeaea)',
      display: "flex",
      justifyContent: "center",
      flexDirection: "column",
      alignItems: "center",
      width: "100%"
  },
  title:{
      fontSize: '25px',
      textAlign: 'center'
  }
}));
