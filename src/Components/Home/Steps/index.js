import React from 'react';
import { Card, CardContent, Typography, Grid, } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import CardIcon from '@material-ui/icons/DirectionsCar';
import BookIcon from '@material-ui/icons/LibraryBooks';

/*CONSTANTES*/
import { styles, } from "./constants";

export default function Steps() {
    const classes = styles();
    return (
        <Grid container xs={12} sm={12} md={12} style={{ marginTop: '10vh', marginBottom: '10vh', padding: 20 }}>
            <Grid xs={12} sm={1} md={1} />
            <Grid xs={12} sm={10} md={10}>
                <Grid container spacing={4} >
                    <Grid item xs={12} sm={4} md={4} container justify='center'>
                        <Card variant="outlined" className={classes.card} >
                            <CardContent>
                                <Typography color="secondary" className={classes.title}>
                                    <strong>Registrate o inicia sesión</strong>
                                </Typography>
                                <Grid xs={12} sm={12} md={12} container justify='center' alignItems='center'>
                                    <PersonIcon style={{ color: '#0f0c29', fontSize:'80px' }} />
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={4} md={4} container justify='center'>
                        <Card variant="outlined" className={classes.card} >
                            <CardContent >
                                <Typography color="secondary" className={classes.title}>
                                    <strong>Gestiona el Alquiler del Vehículo</strong>
                                </Typography>
                                <Grid xs={12} sm={12} md={12} container justify='center' alignItems='center'>
                                    <CardIcon style={{ color: '#0f0c29', fontSize:'80px' }} />
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={4} md={4} container justify='center'>
                        <Card variant="outlined" className={classes.card} >
                            <CardContent >
                                <Typography color="secondary" className={classes.title}>
                                    <strong>Llena el formulario de alquiler</strong>
                                </Typography>
                                <Grid xs={12} sm={12} md={12} container justify='center' alignItems='center'>
                                    <BookIcon style={{ color: '#0f0c29', fontSize:'80px' }} />
                                </Grid>
                            </CardContent>

                        </Card>
                    </Grid>
                </Grid>
            </Grid>
            <Grid xs={12} sm={1} md={1} />
        </Grid>
    )
}
