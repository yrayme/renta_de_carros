import React from 'react';
import { Grid} from '@material-ui/core';
import CarouselData from './CarouselData';
import Steps from './Steps';

function Home() {
    return (
        <Grid container>
            <CarouselData/>
            <Steps/>
        </Grid>
    )
}

export default Home
